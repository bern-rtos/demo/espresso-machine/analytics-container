# Influx & Grafana Docker Container for Espresso Machine Demo

## Prerequisites
Software:
- docker

Hardware:
- Espresso Machine

Configuration
- Host Ethernet: 192.168.10.10/24

## Usage

```bash
docker-compose up
```

## Services and Ports

### Grafana
- URL: http://192.168.10.10:3000 
- User: admin 
- Password: admin 

### InfluxDB
- Port: 8086 (HTTP API)
- User: admin 
- Password: admin 
- Database: influx

